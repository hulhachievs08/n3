<?php
header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  if (!empty($_GET['save'])) {
    print('Спасибо, результаты сохранены.');
  }
  include('form.php');
  exit();
}

$errors = FALSE;

if (empty($_POST['fio'])) {
    print('Заполните имя.<br>');
    $errors = TRUE;
}
else if (!preg_match("/^[а-яА-Я ]+$/u", $_POST['fio'])) {
    print('Недопустимые символы в имени.<br>');
    $errors = TRUE;
}

//email
if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
    print('Проверьте правильность ввода email<br>');
    $errors = TRUE;
}

//year
if (empty($_POST['year_'])) {
    print('Заполните год.<br>');
    $errors = TRUE;
}
else {
    $year_ = $_POST['year_'];
    if (!(is_numeric($year_) && intval($year_) >= 1900 && intval($year_) < 2020)) {
        print("Укажите корректный год.<br>");
        $errors = TRUE;
    }
}

//abilities
$ability_data = ['immort', 'wall', 'levit', 'invis', 'diffur'];
if (empty($_POST['abilities'])) {
    print('Выберите способность<br>');
    $errors = TRUE;
}
else {
    $abilities = $_POST['abilities'];
    foreach ($abilities as $ability) {
        if (!in_array($ability, $ability_data)) {
            print('Недопустимая способность<br>');
            $errors = TRUE;
        }
    }
}
$ability_insert = [];

//XZ insertion
$abilities = $_POST['abilities'];
//
foreach ($ability_data as $ability) {
    $ability_insert[$ability] = in_array($ability, $abilities) ? 1 : 0;
}

//accept
if (empty($_POST['accept_'])) {
    print("Вы не приняли соглашение!<br>");
    $errors = TRUE;
}

if ($errors) {
  exit();
}

$user = 'u40075';
$pass = '8514870';
$db = new PDO('mysql:host=localhost;dbname=u40075', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

try {
  //$stmt = $db->prepare("INSERT INTO application SET fio = ?, email = ?, year = ?, sex = ?, limbs = ?, immort = ?, wall = ?, levit = ?, invis = ?, diffur = ?, text_ = ?, accept_ = ?");
  $stmt = $db->prepare("INSERT INTO application SET fio = ?, email = ?, year_ = ?, sex = ?, limbs = ?, immort = ?, wall = ?, levit = ?, invis = ?, diffur = ?, text_ = ?, accept_ = ?");

  $stmt -> execute([$_POST['fio'], $_POST['email'], intval($_POST[year_]), intval($_POST['sex']),
   intval($_POST['limbs']), $ability_insert['immort'], $ability_insert['wall'],
   $ability_insert['levit'], $ability_insert['invis'], $ability_insert['diffur'], $_POST['text_'],
    intval($_POST['accept_'])]);

  /*$stmt -> execute([$_POST['fio'], $_POST['email'], intval($year_), intval($_POST['sex']),
   intval($_POST['limbs']), $ability_insert['immort'], $ability_insert['wall'],
   $ability_insert['levit'], $ability_insert['invis'], $ability_insert['diffur'], $_POST['text'],
    intval($_POST['accept'])]);
  */
}
catch(PDOException $e) {
  print('Error : ' . $e->getMessage());
  exit();
}

header('Location: ?save=1'); 
