function Calculate() {
    let f_1 = document.getElementsByName("Price");
    let f_2 = document.getElementsByName("Kol");
    let res = document.getElementById("result");
    if ((/^[0-9]+$/).test(f_1[0].value) && (/^[0-9]+$/).test(f_2[0].value)) {
        res.innerHTML = "Итоговая цена покупки " + (f_1[0].value * f_2[0].value);
    } else {
        res.innerHTML = "Перепроверьте данные, возникла ошибка!";
        return false;
    }
}
window.addEventListener("DOMContentLoaded", function () {
    let button = document.getElementById("button_1");
    button.addEventListener("click", Calculate);
});